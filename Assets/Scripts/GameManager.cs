﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public GameObject Player;
    public UIController UIController;

    private int _score;
    public int Score
    {
        get
        {
            return _score;
        }
    }
    private int _numEnemiesOnScene;
    public int NumEnemiesOnScene
    {
        get
        {
            return _numEnemiesOnScene;
        }
    }


    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    void Start()
    {
        _score = 0;
        _numEnemiesOnScene = 0;

        Player = GameObject.Find("Personatge");
        UIController = GameObject.Find("Canvas").GetComponent<UIController>();
    }

    public void IncreaseScore(int increment)
    {
        _score += increment;
    }

    public void IncreaseEnemies()
    {
        _numEnemiesOnScene++;
    }

    public void DecreaseEnemies()
    {
        _numEnemiesOnScene--;
    }
}
