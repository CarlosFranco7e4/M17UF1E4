﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCollitionController : MonoBehaviour
{
    [SerializeField]
    private int scorePoints = 5;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.Instance.IncreaseScore(scorePoints);
            GameObject pl = collision.gameObject;
            pl.GetComponent<Dades>().lifes--;

            if (pl.GetComponent<Dades>().lifes <= 0)
            {
                Debug.Log("GAME OVER");
                SceneManager.LoadScene("PersonatgeEnMoviment");
            }
        }
        GameManager.Instance.DecreaseEnemies();
        Destroy(gameObject);
    }

    private void OnMouseDown()
    {
        GameManager.Instance.DecreaseEnemies();
        GameManager.Instance.IncreaseScore(scorePoints);
        Destroy(gameObject);
    }
}
