﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    private List<GameObject> enemies;

    public Vector2 RandX;
    public Vector2 RandY;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateEnemy", 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CreateEnemy()
    {
        /*GameObject e = Instantiate(enemy, new Vector3(Random.RandomRange(RandX.x, RandX.y), Random.RandomRange(RandY.x, RandY.y), 0), Quaternion.identity);
        enemies.Add(e);*/

        Instantiate(enemy, new Vector3(Random.RandomRange(RandX.x, RandX.y), Random.RandomRange(RandY.x, RandY.y), 0), Quaternion.identity);
        GameManager.Instance.IncreaseEnemies();
    }
}
