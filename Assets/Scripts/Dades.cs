﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Mage,
    Warrior,
    Priest,
    Hunter,
    Warlock,
    DemonHunter,
    Rogue,
    Paladin,
    Druid,
    Shaman
}

public enum Mov
{
    Moviment,
    Stop
}

public class Dades : MonoBehaviour
{
    public string nom;
    public string cognom;
    public float alsada;
    public float velocitat;
    public float distancia;
    public int pes;
    public PlayerKind tipusPersonatge;
    public Mov mov;
    public int lifes;

    void Start()
    {
        Debug.Log($"Hola {nom} {cognom}!");
        Debug.Log($"Tipus {tipusPersonatge}");
    }
}
