﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipusMov
{
    MovimentAuto,
    MovimentInputs
}

public enum Direccio
{
    Dreta,
    Esquerra,
    SenseMoviment
}

public class Animacio : MonoBehaviour
{
    public SpriteRenderer Sp;
    public Sprite[] Sprites;
    private Dades Dat;
    public TipusMov tipusMov;
    public Direccio dir;
    private int contador;

    void Start()
    {
        Sp = gameObject.GetComponent<SpriteRenderer>();
        Dat = gameObject.GetComponent<Dades>();
    }

    void Update()
    {
        switch (Dat.mov)
        {
            case Mov.Moviment:
                Moviment();
                break;
            case Mov.Stop:
                Stop();
                break;
        }
    }

    public void Moviment()
    {
        if (contador < 3)
        {
            contador++;
        }
        else
        {
            contador = 0;
        }
        Sp.sprite = Sprites[contador];
    }

    public void Stop()
    {
        Sp.sprite = Sprites[0];
    }
}
