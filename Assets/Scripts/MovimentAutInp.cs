﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentAutInp : MonoBehaviour
{
    private Animacio Ani;
    private SpriteRenderer Sp;
    private Dades Dat;
    Vector3 inici;
    Vector3 dreta;
    Vector3 esquerra;

    void Start()
    {
        Application.targetFrameRate = 12;
        Sp = gameObject.GetComponent<SpriteRenderer>();
        Dat = gameObject.GetComponent<Dades>();
        Ani = gameObject.GetComponent<Animacio>();
        inici = transform.position;
        dreta = inici + new Vector3(Dat.distancia, 0);
        esquerra = inici - new Vector3(Dat.distancia, 0);
        Ani.Moviment();
    }

    void Update()
    {
        switch (Ani.tipusMov)
        {
            case TipusMov.MovimentAuto:
                if (Ani.dir == Direccio.Dreta)
                {
                    WalkAut(dreta);
                }
                else if (Ani.dir == Direccio.Esquerra)
                {
                    WalkAut(esquerra);
                }
                break;
            case TipusMov.MovimentInputs:
                WalkInputs();
                break;
        }
    }

    private void WalkAut(Vector3 destino)
    {
        transform.position = Vector2.MoveTowards(transform.position, destino, Dat.velocitat / Dat.pes);
        if (transform.position == destino)
        {
            Ani.dir = Direccio.SenseMoviment;
            StartCoroutine(Volta());
        }
    }

    IEnumerator Volta()
    {
        Dat.mov = Mov.Stop;
        yield return new WaitForSeconds(2);
        if (transform.position == dreta)
        {
            Ani.dir = Direccio.Esquerra;
        }
        else
        {
            Ani.dir = Direccio.Dreta;
        }
        Sp.flipX = !Sp.flipX;
        Dat.mov = Mov.Moviment;
    }

    private void WalkInputs()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            Sp.flipX = true;
            Dat.mov = Mov.Moviment;
            transform.position = new Vector3(transform.position.x + (Dat.velocitat / Dat.pes), transform.position.y, transform.position.z);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            Sp.flipX = false;
            Dat.mov = Mov.Moviment;
            transform.position = new Vector3(transform.position.x + (Dat.velocitat / Dat.pes) * -1, transform.position.y, transform.position.z);
        }
        else if (Input.GetAxis("Vertical") > 0)
        {
            Sp.flipX = false;
            Dat.mov = Mov.Moviment;
            transform.position = new Vector3(transform.position.x, transform.position.y + (Dat.velocitat / Dat.pes), transform.position.z);
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            Sp.flipX = false;
            Dat.mov = Mov.Moviment;
            transform.position = new Vector3(transform.position.x, transform.position.y + (Dat.velocitat / Dat.pes) * -1, transform.position.z);
        }
        else
        {
            Dat.mov = Mov.Stop;
        }
    }
}
