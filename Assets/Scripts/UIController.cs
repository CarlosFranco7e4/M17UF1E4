﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    //public Dades DP;
    //public GameObject PlayerGO;

    public Text numberOfFrames;

    public Text PlayerLifesText;
    public Text Score;
    public Text NumEnemiesOnScene;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        PlayerLifesText.text = "Lifes: " + GameManager.Instance.Player.GetComponent<Dades>().lifes;
        Score.text = "Score: " + GameManager.Instance.Score;
        NumEnemiesOnScene.text = "Enemies: " + GameManager.Instance.NumEnemiesOnScene;
    }
}
