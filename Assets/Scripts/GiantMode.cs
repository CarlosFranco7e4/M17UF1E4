﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EstatGiant
{
    Petit,
    Creixent,
    Gran,
    Decreixent,
    Cooldown
}

public class GiantMode : MonoBehaviour
{
    private EstatGiant Estat;
    private Dades Dat;
    private Vector3 original;
    private Vector3 gegant;
    private int tempsGegant = 120;
    private int cd = 240;
    void Start()
    {
        Dat = gameObject.GetComponent<Dades>();
        original = transform.localScale;
        gegant = transform.localScale * (Dat.alsada * 3);
    }

    void Update()
    {
        switch (Estat)
        {
            case EstatGiant.Petit:
                petit();
                break;
            case EstatGiant.Creixent:
                creixent();
                break;
            case EstatGiant.Gran:
                gran();
                break;
            case EstatGiant.Decreixent:
                decreixent();
                break;
            case EstatGiant.Cooldown:
                cooldown();
                break;
        }
    }

    private void petit()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Estat = EstatGiant.Creixent;
        }
    }

    private void creixent()
    {
        transform.localScale += new Vector3(1,1,1) * Time.deltaTime * 2;
        if (transform.localScale.x >= gegant.x)
        {
            Estat = EstatGiant.Gran;
        }
    }

    private void gran()
    {
        Estat = EstatGiant.Decreixent;
    }

    private void decreixent()
    {
        tempsGegant--;
        if (tempsGegant < 0)
        {
            transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime * 5;
            if (transform.localScale.x <= original.x)
            {
                Estat = EstatGiant.Cooldown;
                tempsGegant = 120;
            }
        }
    }

    private void cooldown()
    {
        cd--;
        if (cd <= 0)
        {
            Estat = EstatGiant.Petit;
            cd = 240;
        }
    }
}
